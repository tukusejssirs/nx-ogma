import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { OgmaLogger, OgmaService } from '@ogma/nestjs-module';
import {lastValueFrom} from 'rxjs'

@Injectable()
export class AppService {
  constructor(
    @OgmaLogger(AppService.name) private readonly logger: OgmaService,
    @Inject('MICROSERVICE') private readonly microserviceClient: ClientProxy
   ) {}

  async getData(): Promise<{ message: string }> {
    this.logger.log('test ogma logger', {context: AppService.name})
    // this.logger.log('test ogma logger')
    await lastValueFrom(this.microserviceClient.send<unknown>({cmd: 'getData'}, {}))
    return { message: 'Welcome to test-app!' };
  }
}
