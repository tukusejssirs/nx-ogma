import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { OgmaModule } from '@ogma/nestjs-module';
import { FastifyParser } from '@ogma/platform-fastify';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    OgmaModule.forRoot({
      interceptor: {
        http: FastifyParser
      },
      service: {
        application: 'NxOgma',
        color: true,
        json: false
      }
    }),
    OgmaModule.forFeature(AppService.name),
    ClientsModule.register([
      {
        name: 'MICROSERVICE',
        options: {
          host: '127.0.0.1',
          port: 4444
        },
        transport: Transport.TCP
      }
    ])
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
