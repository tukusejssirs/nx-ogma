import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { OgmaService } from '@ogma/nestjs-module';

import { AppModule } from './app/app.module';

async function bootstrap() {
  // const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter(), {logger: OgmaService as unknown as Logger})
  const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter())
  const globalPrefix = 'api';
  const logger = app.get<OgmaService>(OgmaService);
  const port = process.env.PORT || 3333;

  app.useLogger(logger);
  app.setGlobalPrefix(globalPrefix);
  await app.listen(port);

  logger.log(`🚀 Application is running on: http://localhost:${port}/${globalPrefix}`);
}

bootstrap();
