import { Injectable } from '@nestjs/common';
import { OgmaLogger, OgmaService } from '@ogma/nestjs-module';

@Injectable()
export class SomeService {
  constructor(@OgmaLogger(SomeService.name) private readonly logger: OgmaService) {}

  getData(): { message: string } {
    this.logger.log('test ogma logger', {context: SomeService.name});
    // this.logger.log('test ogma logger');
    return { message: 'Welcome to microservice!' };
  }
}
