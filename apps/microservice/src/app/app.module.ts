import { Module } from '@nestjs/common';
import { OgmaModule } from '@ogma/nestjs-module';
import { FastifyParser } from '@ogma/platform-fastify';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SomeService } from './some.service';

@Module({
  imports: [
    OgmaModule.forRoot({
      interceptor: {
        http: FastifyParser
      },
      service: {
        application: 'Microservice',
        color: true,
        json: false
      }
    }),
    OgmaModule.forFeature(SomeService.name)
  ],
  controllers: [AppController],
  providers: [AppService, SomeService],
})
export class AppModule {}
