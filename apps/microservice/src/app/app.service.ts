import { Injectable } from '@nestjs/common';
import { OgmaLogger, OgmaService } from '@ogma/nestjs-module';

@Injectable()
export class AppService {
  constructor(@OgmaLogger(AppService.name) private readonly logger: OgmaService) {}

  getData(): { message: string } {
    // this.logger.log('test ogma logger', {context: AppService.name})
    this.logger.log('test ogma logger')
    return { message: 'Welcome to microservice!' };
  }
}
