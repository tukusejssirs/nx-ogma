import { NestFactory } from '@nestjs/core';
import { ClientOptions, MicroserviceOptions, Transport } from '@nestjs/microservices'
import { OgmaService } from '@ogma/nestjs-module'

import { AppModule } from './app/app.module';

async function bootstrap(): Promise<void> {
  const config: ClientOptions = {
    options: {
      host: '127.0.0.1',
      port: 4444
    },
    transport: Transport.TCP
  }

  const app = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, config)
  const logger = app.get<OgmaService>(OgmaService);

  await app.listen()
  app.useLogger(logger);

  logger.log(`🚀 Application is running on: ${Transport[config.transport].toLowerCase()}://${config.options.host}:${config.options.port}`);
}

bootstrap().catch(e => console.log(e))
